# NAME

HPC::Runner::Threads - Job submission using threads

# VERSION

Version 0.01

# SYNOPSIS

Use Parallel::ForkManager to run arbitrary bash commands

# Attributes

## twait

How frequently to test for a thread having exited the queue in seconds. Defaults to once every 60 seconds. If your jobs are very fast, you may want to decrease this number, or vice versa if they are very long.

## threads

This uses Parallel::ForkManager to deploy the threads. If you wish to use something else you must redefine it here.

# SUBROUTINES/METHODS

## go

This is the main application. It starts the logging, build the threads, parses the file, runs the commands, and finishes logging.

## parse\_file\_threads

Parse the file of commands and send each command off to the queue.

\#TODO
\#Merge mce/threads subroutines

## build\_threads

This is the command to build the threads. To change this just add a build\_threads method in your script.

    $self->threads->run_on_wait(
        sub {
            $self->log->debug("** Queue full. Waiting for one process to end ...");
        },
        $self->twait,
    );

or

    package Main;
    extends 'HPC::Runner::Threads';

    sub build_threads {

        $self->threads->run_on_wait(
            sub {
                $self->log->debug("** This is my custom message");
            },
            $self->twait,
        );
    }

# See Also

[Parallel::ForkManager](https://metacpan.org/pod/Parallel::ForkManager), [HPC::Runner::GnuParallel](https://metacpan.org/pod/HPC::Runner::GnuParallel)

# AUTHOR

Jillian Rowe, `<jillian.e.rowe at gmail.com>`

# BUGS

Please report any bugs or feature requests to `bug-runner-init at rt.cpan.org`, or through
the web interface at [http://rt.cpan.org/NoAuth/ReportBug.html?Queue=HPC-Runner-Threads](http://rt.cpan.org/NoAuth/ReportBug.html?Queue=HPC-Runner-Threads).  I will be notified, and then you'll
automatically be notified of progress on your bug as I make changes.

# SUPPORT

You can find documentation for this module with the perldoc command.

    perldoc HPC::Runner::Threads

You can also look for information at:

- RT: CPAN's request tracker (report bugs here)

    [http://rt.cpan.org/NoAuth/Bugs.html?Dist=HPC-Runner-Threads](http://rt.cpan.org/NoAuth/Bugs.html?Dist=HPC-Runner-Threads)

- AnnoCPAN: Annotated CPAN documentation

    [http://annocpan.org/dist/HPC-Runner-Threads](http://annocpan.org/dist/HPC-Runner-Threads)

- CPAN Ratings

    [http://cpanratings.perl.org/d/HPC-Runner-Threads](http://cpanratings.perl.org/d/HPC-Runner-Threads)

- Search CPAN

    [http://search.cpan.org/dist/HPC-Runner-Threads/](http://search.cpan.org/dist/HPC-Runner-Threads/)

# Acknowledgements

This module was originally developed at and for Weill Cornell Medical
College in Qatar within ITS Advanced Computing Team. With approval from
WCMC-Q, this information was generalized and put on github, for which
the authors would like to express their gratitude.

# LICENSE AND COPYRIGHT

Copyright 2014 Weill Cornell Medical College Qatar.

This program is free software; you can redistribute it and/or modify it
under the terms of the the Artistic License (2.0). You may obtain a
copy of the full license at:

[http://www.perlfoundation.org/artistic\_license\_2\_0](http://www.perlfoundation.org/artistic_license_2_0)

Any use, modification, and distribution of the Standard or Modified
Versions is governed by this Artistic License. By using, modifying or
distributing the Package, you accept this license. Do not use, modify,
or distribute the Package, if you do not accept this license.

If your Modified Version has been derived from a Modified Version made
by someone other than you, you are nevertheless required to ensure that
your Modified Version complies with the requirements of this license.

This license does not grant you the right to use any trademark, service
mark, tradename, or logo of the Copyright Holder.

This license includes the non-exclusive, worldwide, free-of-charge
patent license to make, have made, use, offer to sell, sell, import and
otherwise transfer the Package with respect to any patent claims
licensable by the Copyright Holder that are necessarily infringed by the
Package. If you institute patent litigation (including a cross-claim or
counterclaim) against any party alleging that the Package constitutes
direct or contributory patent infringement, then this Artistic License
to you shall terminate on the date that such litigation is filed.

Disclaimer of Warranty: THE PACKAGE IS PROVIDED BY THE COPYRIGHT HOLDER
AND CONTRIBUTORS "AS IS' AND WITHOUT ANY EXPRESS OR IMPLIED WARRANTIES.
THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
PURPOSE, OR NON-INFRINGEMENT ARE DISCLAIMED TO THE EXTENT PERMITTED BY
YOUR LOCAL LAW. UNLESS REQUIRED BY LAW, NO COPYRIGHT HOLDER OR
CONTRIBUTOR WILL BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, OR
CONSEQUENTIAL DAMAGES ARISING IN ANY WAY OUT OF THE USE OF THE PACKAGE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
